{
  "properties" : { },
  "id" : "eb2b08bc7fe74036867e8dc5f2c0e8f8",
  "script" : null,
  "groupId" : "6f106ebdee21489db34b956f7770ff03",
  "name" : "选择组件",
  "createTime" : null,
  "updateTime" : 1651902325727,
  "lock" : "0",
  "createBy" : null,
  "updateBy" : null,
  "path" : "/select",
  "method" : "GET",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": 200,\n    \"message\": \"success\",\n    \"data\": [{\n        \"value\": \"55ff62aa20144b7bb5c6dfb5d76c8139\",\n        \"label\": \"数据管理(/data)\",\n        \"children\": [{\n            \"value\": \"eb5dbed949de4f50ba4bf59f483252a5\",\n            \"label\": \"测试生成(/test)\",\n            \"children\": [{\n                \"id\": \"data-test-list\",\n                \"label\": \"列表(/list)\"\n            }]\n        }]\n    }],\n    \"timestamp\": 1651902176780,\n    \"executeTime\": 9\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : {
    "name" : "",
    "value" : "",
    "description" : "",
    "required" : false,
    "dataType" : "Object",
    "type" : null,
    "defaultValue" : null,
    "validateType" : "",
    "error" : "",
    "expression" : "",
    "children" : [ {
      "name" : "code",
      "value" : "200",
      "description" : "",
      "required" : false,
      "dataType" : "Integer",
      "type" : null,
      "defaultValue" : null,
      "validateType" : "",
      "error" : "",
      "expression" : "",
      "children" : [ ]
    }, {
      "name" : "message",
      "value" : "success",
      "description" : "",
      "required" : false,
      "dataType" : "String",
      "type" : null,
      "defaultValue" : null,
      "validateType" : "",
      "error" : "",
      "expression" : "",
      "children" : [ ]
    }, {
      "name" : "data",
      "value" : "",
      "description" : "",
      "required" : false,
      "dataType" : "Object",
      "type" : null,
      "defaultValue" : null,
      "validateType" : "",
      "error" : "",
      "expression" : "",
      "children" : [ {
        "name" : "",
        "value" : "",
        "description" : "",
        "required" : false,
        "dataType" : "Object",
        "type" : null,
        "defaultValue" : null,
        "validateType" : "",
        "error" : "",
        "expression" : "",
        "children" : [ {
          "name" : "value",
          "value" : "55ff62aa20144b7bb5c6dfb5d76c8139",
          "description" : "",
          "required" : false,
          "dataType" : "String",
          "type" : null,
          "defaultValue" : null,
          "validateType" : "",
          "error" : "",
          "expression" : "",
          "children" : [ ]
        }, {
          "name" : "label",
          "value" : "数据管理(/data)",
          "description" : "",
          "required" : false,
          "dataType" : "String",
          "type" : null,
          "defaultValue" : null,
          "validateType" : "",
          "error" : "",
          "expression" : "",
          "children" : [ ]
        }, {
          "name" : "children",
          "value" : "",
          "description" : "",
          "required" : false,
          "dataType" : "Array",
          "type" : null,
          "defaultValue" : null,
          "validateType" : "",
          "error" : "",
          "expression" : "",
          "children" : [ {
            "name" : "",
            "value" : "",
            "description" : "",
            "required" : false,
            "dataType" : "Object",
            "type" : null,
            "defaultValue" : null,
            "validateType" : "",
            "error" : "",
            "expression" : "",
            "children" : [ {
              "name" : "value",
              "value" : "eb5dbed949de4f50ba4bf59f483252a5",
              "description" : "",
              "required" : false,
              "dataType" : "String",
              "type" : null,
              "defaultValue" : null,
              "validateType" : "",
              "error" : "",
              "expression" : "",
              "children" : [ ]
            }, {
              "name" : "label",
              "value" : "测试生成(/test)",
              "description" : "",
              "required" : false,
              "dataType" : "String",
              "type" : null,
              "defaultValue" : null,
              "validateType" : "",
              "error" : "",
              "expression" : "",
              "children" : [ ]
            }, {
              "name" : "children",
              "value" : "",
              "description" : "",
              "required" : false,
              "dataType" : "Array",
              "type" : null,
              "defaultValue" : null,
              "validateType" : "",
              "error" : "",
              "expression" : "",
              "children" : [ {
                "name" : "",
                "value" : "",
                "description" : "",
                "required" : false,
                "dataType" : "Object",
                "type" : null,
                "defaultValue" : null,
                "validateType" : "",
                "error" : "",
                "expression" : "",
                "children" : [ {
                  "name" : "id",
                  "value" : "data-test-list",
                  "description" : "",
                  "required" : false,
                  "dataType" : "String",
                  "type" : null,
                  "defaultValue" : null,
                  "validateType" : "",
                  "error" : "",
                  "expression" : "",
                  "children" : [ ]
                }, {
                  "name" : "label",
                  "value" : "列表(/list)",
                  "description" : "",
                  "required" : false,
                  "dataType" : "String",
                  "type" : null,
                  "defaultValue" : null,
                  "validateType" : "",
                  "error" : "",
                  "expression" : "",
                  "children" : [ ]
                } ]
              } ]
            } ]
          } ]
        } ]
      } ]
    }, {
      "name" : "timestamp",
      "value" : "1651902176780",
      "description" : "",
      "required" : false,
      "dataType" : "Long",
      "type" : null,
      "defaultValue" : null,
      "validateType" : "",
      "error" : "",
      "expression" : "",
      "children" : [ ]
    }, {
      "name" : "executeTime",
      "value" : "9",
      "description" : "",
      "required" : false,
      "dataType" : "Integer",
      "type" : null,
      "defaultValue" : null,
      "validateType" : "",
      "error" : "",
      "expression" : "",
      "children" : [ ]
    } ]
  }
}
================================
import org.ssssssss.magicapi.utils.PathUtils
import org.ssssssss.magicapi.core.service.MagicResourceService
var getFiles = (groupId) => {
    return MagicResourceService.listFiles(groupId).map(file => { 
        value: PathUtils.replaceSlash(String.format("%s/%s", MagicResourceService.getGroupPath(file.groupId), file.path)).replace(/^\//,'').replace(/\/\//, '/').replace('/', '-'),
        label: `${file.name}(${file.path})`
    })
}
var toTree = (children) => {
    var treeData = []
    children.forEach(it => {
        var chi = {}
        chi.value = it.node.id
        chi.label = `${it.node.name}(${it.node.path})`
        if(it.children.length > 0){
            chi.children = toTree(it.children)
            var files = getFiles(it.node.id)
            if(files.length > 0){
                chi.children.push(...files)
            }
        }else{
            var files = getFiles(it.node.id)
            if(files.length > 0){
                chi.children = files
            }
        }
        treeData.push(chi)
    }) 
    return treeData
}
return toTree(MagicResourceService.tree('component').children)